# coding=utf-8
"""
Module for joint base class and primitive joints.
"""


__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import math3d as m3d
import numpy as np

class Joint(object):
    def __call__(self, q):
        raise NotImplemented
    def transform(self, q=None):
        self(q)
        return self._m3dXForm
    def array(self, q):
        return self(q)


class DHRevJoint(Joint):
    """A standard Denavit-Hartenberg revolute joint and link transform."""

    def __init__(self, a, d, alpha):
        (self._a, self._d, self._alpha) = (a, d, alpha)
        self._calpha = np.cos(self._alpha)
        self._salpha = np.sin(self._alpha)
        self._m3dXForm = m3d.Transform()
        self._xform = self._m3dXForm._data
        # // Row 2 is static
        self._xform[2,1] = self._salpha
        self._xform[2,2] = self._calpha
        self._xform[2,3] = self._d
        self.array = self.__call__
        self._q = 0.0

    def __call__(self, q):
        if not q is None and q != self._q:
            self._q = q
            cq = np.cos(q)
            sq = np.sin(q)
            # // Col 0
            self._xform[0,0] = cq
            self._xform[1,0] = sq
            # // Col 1
            self._xform[0,1] = -sq * self._calpha
            self._xform[1,1] = cq * self._calpha
            # // Col 2
            self._xform[0,2] = sq * self._salpha
            self._xform[1,2] = -cq * self._salpha
            # // Col 3
            self._xform[0,3] = self._a * cq
            self._xform[1,3] = self._a * sq
        return self._xform


        
class RevoluteJoint(Joint):
    """A simple rotational joint, connecting the exterior to the
    interior reference frame by a rotation around the z-axis."""

    def __init__(self):
        self._m3dXForm = m3d.Transform()
        self._xform = self._m3dXForm._data
        self.array = self.__call__
        self._q = 0.0
        
    def __call__(self, q):
        if not q is None and q != self._q:
            self._q = q
            cq = np.cos(q)
            sq = np.sin(q)
            #self._xform[:2,:2] = [[cq, -sq], [sq, cq]]
            self._xform[0,0] = cq
            self._xform[0,1] = -sq
            self._xform[1,0] = sq
            self._xform[1,1] = cq
        return self._xform


class PrismaticJoint(Joint):
    """A simple prismatic joint sliding the exterior reference frame
    on the z-axis of the interior."""

    def __init__(self):
        self._m3dXForm = m3d.Transform()
        self._xform = self._m3dXForm._data
        self.array = self.__call__
        self._q = 0.0
        
    def __call__(self, q):
        if not q is None and q != self._q:
            self._q = q
            self._xform[2,3] = q
        return self._xform
