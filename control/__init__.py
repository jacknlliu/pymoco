# coding=utf-8

"""
Initialization of control package.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import numpy as np

inf_norm = lambda a: np.linalg.norm(a, np.inf)

llchost = ''
llc_in_port = 20021

