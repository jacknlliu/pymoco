# coding=utf-8

"""
Module for correction motion control base class.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading 
import traceback
import time
import math

import numpy
import math3d
import math3d.interpolation

from ..kinematics import framecomputer
from .toollinearcontroller import ToolLinearController

class _Corrector(object):
    """A corrector class for holding the target correction and
    managing the updates towards it.
    """
    
    def __init__(self):
        self._corrBase = math3d.Transform()
        self._corr_target = math3d.Transform()
        self._start_time = 0.0
        self._end_time = 1.0
        self._delta_t = 1.0
        self._inv_delta_t = 1.0 / self._delta_t
        self._aborted = False
        self._abort_time = 0.0

    def _l_time(self, t=None):
        if t is None:
            t = time.time()
        return self._inv_delta_t * (t - self._start_time)

    def abort(self):
        if not self._aborted:
            self._aborted=True
            self._abort_time = time.time()
            self._abort_corr = self._corr_interpolator(
                self._l_time(self._abort_time))
            
    def get_target(self):
        """Return the current correction target."""
        return self._corr_target
    def set_target(self, corr_target=math3d.Transform(),
                      delta_t=1.0, start_time=None):
        """Set the correction target. 'corr_target' must be a
        m3d.Transform. 'delta_t' specifies the time, in seconds, over
        which the correction should be obtained. 'start_time'
        specifies, in epoch time, when the correction should start to
        take effect.
        """
        if start_time is None:
            start_time = time.time()
        self._start_time = start_time
        self._delta_t = delta_t
        self._inv_delta_t = 1.0/delta_t
        self._end_time = self._start_time + self._delta_t
        self._corrBase = self._corr_target
        self._corr_target = corr_target.copy()
        self._corr_interpolator = math3d.interpolation.SE3Interpolation(
            self._corrBase, self._corr_target)
        self._aborted = False
        self._abort_time = 0.0
        
    def __call__(self, t=None):
        return self.get_correction(t)
    
    def is_idle(self, t=None):        
        if t is None:
            t = time.time()
        return t < self._start_time or t > self._end_time
            
    def get_current_correction(self, t=None):
        if t is None:
            t = time.time()
        if self._aborted:
            if t >= self._abort_time:
                return self._abort_corr
        if t >= self._end_time:
            return self._corr_target
        elif t <= self._start_time:
            return self._corrBase
        else:
            return self._corr_interpolator(self._l_time(t))
    current_correction = property(get_current_correction)
        

class CorrectionController(ToolLinearController,
                           threading.Thread):
    """Class implementing a linear tool controller with real-time
    trajectory correction capability.
    """

    class Error(Exception):
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        ToolLinearController.__init__(self, **kwargs)
        self._corr_lock = threading.Lock()
        self._corrector = _Corrector()
        self._corrXform = self._corrector()
        
    def _set_correction(self, corrXform=math3d.Transform(), \
                          delta_t=0.5, start_time=None):
        if start_time is None:
            start_time = time.time()
        with self._corr_lock:
            self._corrector.set_target(corr_target=corrXform,
                                          delta_t=delta_t, start_time=start_time)

    def abort_correction(self):
        with self._corr_lock:
            self._corrector.abort()
            
    def get_correction_target(self):
        with self._corr_lock:
            return self._corrector.get_target()

    def get_correctionEnd_time(self):
        with self._corr_lock:
            return self._corrector._end_time

    def get_correction_start_time(self):
        with self._corr_lock:
            return self._corrector._start_time

    def get_current_correction(self):
        """Get the currently applied correction."""
        with self._corr_lock:
            return self._corrector()
    current_correction = property(get_current_correction)

    def _ramp_step(self):
        with self._llc_lock:
            ctf = self._frame_comp(self._rob_fac.cmd_joint_pos)
            invjac = self._frame_comp.inverse_jacobian()
            llc_time = self._llc_time ## Current LLC time
        period_est = self._rob_fac.control_period
        with self._corr_lock:
            self._last_corrXform = self._corrXform
            self._corrXform = self._corrector(llc_time)
        last_corrXform = self._last_corrXform
        corrXform = self._corrXform
        pctf = self._un_correct(last_corrXform, ctf)
        pathPos = self._trf0._v.dist(pctf._v)
        if self._abort and not self._aborting:
            self._aborting = True
            self._brakePos = min(pathPos, self._brakePos)
            self._stopPos = min(self._path_length, self._acc_length + pathPos)
        lastPos = self._lastSetPos
        ## Determine if we are ramping up, going flat, or ramping down.
        if pathPos <= self._acc_length \
               and pathPos < self._brakePos:
            ## Ramping up
            v = min(self._target_speed, math.sqrt(2 * lastPos * self._ramp_accel))
            if v == 0.0:
                ## Make sure we actually get an initial speed
                v = self._ramp_accel * period_est
            newPathPos = lastPos + period_est * v
            if self._log_level >= 5:
                self._log('Ramping UP at position %f and speed %f'%(lastPos, v), 5)
        if pathPos >= self._brakePos:
            ## Ramping down
            vsqr = 2 * (self._stopPos - pathPos) * self._ramp_accel
            if vsqr <= 0:
                v = 0.0
                ## We're done (should be!)
                if self._log_level >= 3:
                    self._log('Task reached', 3)
                newPathPos = lastPos
            else:
                v = math.sqrt(vsqr)
                newPathPos = lastPos + period_est * v
            if self._log_level >= 5:
                self._log('Ramping DOWN at position %f and speed %f' %
                      (pathPos, v), 5)
        if pathPos < self._brakePos and pathPos > self._acc_length:
            ## Going on target velocity
            newPathPos = lastPos + period_est * self._target_speed
            if self._log_level >= 5:
                self._log('%.4f : Constant speed at position %f and speed %f' %
                      (llc_time, pathPos, self._target_speed), 5)
        ## Compute joint step to new path position
        if pathPos >= self._stopPos:
            if self._log_level >= 3:
                self._log('!! WARNING : Got path position exceeding ' +
                          'path length by %f m!!' % (pathPos-self._stopPos), 3)
        if newPathPos < pathPos:
            if self._log_level >= 3:
                self._log('!! WARNING : Got new path position ' +
                          'preceeding current !!', 3)
        self._lastSetPos = newPathPos
        ttf = self._tli(min(1, newPathPos / self._path_length))
        dtrf = framecomputer.trfdisp(ctf, self._correct(corrXform, ttf))
        dq = numpy.dot(invjac, dtrf)
        ## Execute the joint step
        self._rob_fac.cmd_joint_increment = dq
        # dqs = self._rob_fac.cmd_joint_increment
        # if numpy.sum((dq - dqs) ** 2) > 0.0001:
        #     pctfnew = self._un_correct(corrXform, self._frame_comp(dqs + self._rob_fac.cmd_joint_pos))
        #     self._lastSetPos = newPathPos = self._trf0._v.dist(pctfnew._v)
        #     if self._log_level >= 3:
        #         self._log('Joint increment retarded due to speed limit', 3)
        ## If reached, go idle
        if pathPos >= self._stopPos - 0.001 and self._corrector.is_idle():
            self._tReached = True
            self._set_idle()
        ## Test if the computation went past the deadline
        if self._llc_event.is_set():
            if self._log_level >= 2:
                self._log('!!! LLC event happened before this cycle. !!!', 2)
            if self._dropFrames:
                if self._log_level >= 2:
                    self._log('!!! Dropping control frame. !!!', 2)
                self._llc_event.clear()

    def _correct(self, corr, xform):
        raise NotImplemented

    def _un_correct(self, corr, xform):
        raise NotImplemented

    def set_target_pose(self, 
                      target_pose, target_speed=0.25,
                      ramp_accel=1.0, do_scale=True, express_frame='Base'):
        """Perform a linear motion from current tool pose to the given
        'target_pose', with 'target_speed' [m/s] and a 'rampAcc'
        [m/s^2] acceleration for ramping the speed up and down.
        """
        ## Setup, check, and start task
        with self._task_lock:
            if self.is_running():
                # Reject setting new target while running.
                taskaccepted = False
            else:
                self._do_scale = do_scale
                self._lastSetPos = 0
                self._tReached = False
                self._abort = False
                self._aborting = False
                self._target_speed = target_speed
                self._ramp_accel = rampAcc
                corr = self._corrector.current_correction
                self._trf0 = self._un_correct(corr, self._frame_comp(self._rob_fac.cmd_joint_pos))
                self._trf1 = math3d.Transform(target_pose)
                self._log('Current pose : %s' % str(self._trf0), 4)
                self._log('Target pose : %s' % str(self._trf1), 4)
                self._path_length = self._trf0._v.dist(self._trf1._v)
                self._stopPos = self._path_length
                if self._path_length < 0.0001:
                    taskaccepted = False
                else:
                    self._tli = math3d.interpolation.SE3Interpolation(
                        self._trf0, self._trf1)                
                    self._acc_length = 0.5*self._target_speed**2/self._ramp_accel
                    self._brakePos = self._path_length-self._acc_length \
                                     - self._target_speed * self._rob_fac.control_period
                    self._log('Starting : BP=%.3f PL=%.3f AL=%.3f' %
                              (self._brakePos, self._path_length,
                               self._acc_length), 3)
                    if 2*self._acc_length > self._path_length:
                        self._brakePos = 0.5*self._path_length
                    self._tstart = time.time()
                    self._control_step = self._ramp_step
                    self._set_running()
                    taskaccepted = True
        return taskaccepted
