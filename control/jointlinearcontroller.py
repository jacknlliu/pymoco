# coding=utf-8

"""
Module for linear joint motion control.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import traceback

import numpy as np

from .controller import Controller 

from . import inf_norm

class JointLinearController(Controller, threading.Thread):
    """ Simple class to perform linear interpolation and take target
    positions in tool flange coordinates.
    """

    class Error(Exception):
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message
        
    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        self.daemon = True
        Controller.__init__(self, **kwargs)
        self._log_level = kwargs.get('log_level', 2)
        self._stop_flag = False
        self.__running = threading.Event()
        self.__idle = threading.Event()
        self._q_target = None

    def _log(self, msg, level=1):
        if level <= self._log_level:
            print(self.__class__.__name__ + '::' \
                  + traceback.extract_stack(None, 2)[-2][2] \
                  + ' : '+msg)
    
    def _set_running(self):
        """Switch to running mode."""
        self.__running.set()
        self.__idle.clear()

    def _set_idle(self):
        """Switch to idle mode."""
        self.__running.clear()
        self.__idle.set()

    def wait_for_idle(self, timeout=None):
        """Block until the idle flag is set, signalling that the
        controller is then idle.
        """
        if timeout is None:
            self.__idle.wait()
        else:
            self.__idle.wait(timeout)
        return self.__idle.is_set()

    def abort(self, wait=True):
        """Abort the current operation."""
        self._abort = True
        if wait:
            while True:
                self._rob_fac.wait_for_control()
                if (np.linalg.norm(self._rob_fac.cmd_joint_increment) 
                    / self._rob_fac.control_period) < 1.e-3:
                    break

    def is_running(self):
        return self.__running.is_set()

    def is_idle(self):
        return not self.__running.is_set()

    def set_joint_target(self, q_target, target_speed=1.0, ramp_accel=3.0):
        if not type(q_target) == np.ndarray:
            q_target =np.array(q_target)
        self._target_speed = target_speed
        self._ramp_accel = ramp_accel
        self._speed = min(self._target_speed, self._ramp_accel * self._rob_fac.control_period)
        self._q_target = q_target
        self._path_length = inf_norm(self._q_target - self._rob_fac.cmd_joint_pos)
        self._ramping_down = False
        self._aborting = False
        self._abort = False
        self._set_running()
        return True

    def run(self):
        self.resume()
        self._stop_flag = False
        while not self._stop_flag:
            ## Receive LLC data
            self._llc_event.wait()
            self._llc_event.clear()
            ## Compose new target
            if self.is_running():
                period_est = self._rob_fac.control_period
                delta_q = self._q_target - self._rob_fac.cmd_joint_pos
                if self._abort and not self._aborting:
                    self._aborting = True
                    ## Recompute q_target and delta_q
                    delta_q *= self._speed**2 / (2 * self._ramp_accel * inf_norm(delta_q))
                    self._q_target = self._rob_fac.cmd_joint_pos + delta_q
                remaining_path = inf_norm(delta_q)
                if 2 * remaining_path * self._ramp_accel <= (self._speed + 0.01)**2:
                    if not self._ramping_down:
                        if self._log_level >= 5:
                            self._log('Starting ramp-down', 4)
                    self._ramping_down = True
                    self._speed -= self._ramp_accel * period_est
                    if self._log_level >= 5:
                        self._log('Ramping down to %f' % self._speed, 5)
                elif self._speed <= self._target_speed and not self._ramping_down:
                    self._speed += self._ramp_accel * period_est
                    if self._speed > self._target_speed:
                        self._speed = self._target_speed
                dq = self._speed * period_est * delta_q/remaining_path
                dq_inf = inf_norm(dq) 
                if (dq_inf< remaining_path and
                    remaining_path > 0.001 and
                    dq_inf>0.0001):
                    ## Step is still smaller than remaining path,
                    ## the remaining path is still significant,
                    ## and the step proposed is still significant.
                    self._rob_fac.cmd_joint_increment = dq
                    if self._log_level >= 6:
                        self._log('Sent increment to actuators: %s' % str(dq), 6)
                else:
                    self._rob_fac.cmd_joint_increment = delta_q
                    self._set_idle()
                    self._log('Done', 2)
            else: # // If not running.
                self._rob_fac.cmd_joint_increment = self._dq_zero
        self._exit_event.set()
        self.suspend()
        
    def stop(self):
        """ Stop thread activity."""
        self._stop_flag = True
        # Make sure to wake up
        self._llc_event.set()
        # Wait for controller to complete
        self._exit_event.wait()
