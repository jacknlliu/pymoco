# coding=utf-8

"""
Module for path correction control on linear motion.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

from .correctioncontroller import CorrectionController

class PathCorrectionController(CorrectionController):
    ''' Class implementing a linear tool controller with real-time
    path correction capability.'''

    class Error(Exception):
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        CorrectionController.__init__(self, **kwargs)
        
    def _correct(self, corr, xform):
        return corr * xform

    def _un_correct(self, corr, xform):
        return corr.inverse * xform

    set_path_correction = CorrectionController._set_correction
