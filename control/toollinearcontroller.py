# coding=utf-8

"""
Module for implementing linear tool motion control.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading 
import traceback
import time
import math
import gc

import numpy as np
import math3d as m3d
import math3d.interpolation

from .kinematicscontroller import KinematicsController
from ..kinematics import framecomputer

class ToolLinearController(KinematicsController,
                           threading.Thread):
    """Class implementing a linear tool controller."""

    class Error(Exception):
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        """Create a tool linear controller. Supported 'kwargs' with
        default values in parentheses: 'drop_frames'(False),
        'linear_speed'(0.25), 'angular_speed'(0.75),
        'ramp_accel'(1.5), 'singular_cutoff'(1.0e-8),
        'stopping_accuracy'(1.0e-5).
        """
        KinematicsController.__init__(self, **kwargs)
        threading.Thread.__init__(self)
        self.daemon = True
        self._drop_frames = kwargs.get('drop_frames', False)
        self._sample_times = kwargs.get('sample_times', 0)
        self._dflt_lin_spd = kwargs.get('linear_speed', 0.25)
        self._dflt_ang_spd = kwargs.get('angular_speed', 0.75)
        self._dflt_ramp_acc = kwargs.get('ramp_accel', 1.5)
        if self._sample_times > 0:
            self._comp_times = -np.ones(self._sample_times, dtype=float)
        self._singular_cutoff = kwargs.get('singular_cutoff', 1.0e-8)
        # // The tolerated tool point deviation from the commanded trajectory point.
        self._trajectory_tolerance = kwargs.get('trajectory_tolerance', 0.0)
        ## The accuracy to which the end point of a task should be
        ## obtained. (Default 10um or 10urad)
        self._stopping_accuracy = kwargs.get('stopping_accuracy', 1.0e-5)
        # // Estimate of the length of the arm
        self._arm_scale = 0.667 * np.sum(
            [lt.pos.length for lt in self._rob_def._link_xforms])
        self.__running = threading.Event()
        self.__idle = threading.Event()
        self._stop_flag = False
        self._task_lock = threading.Lock()

    def _log(self, msg, log_level=1):
        if log_level <= self._log_level:
            print(str(log_level) + ' : ' + self.__class__.__name__ + '::' 
                  + traceback.extract_stack(None, 2)[-2][2] 
                  + ' : ' + msg)
    
    def _ltime(self, rt=None):
        if rt == None:
            rt = time.time()
        return self._invDT*(rt-self._tstart)

    def _set_running(self):
        """Switch to running mode."""
        self.__running.set()
        self.__idle.clear()

    def _set_idle(self):
        """Switch to idle mode."""
        self.__running.clear()
        self.__idle.set()

    def wait_for_idle(self, timeout=None):
        """Block until the idle flag is set, signalling that the
        controller is then idle.
        """
        if timeout is None:
            self.__idle.wait()
        else:
            self.__idle.wait(timeout)
        return self.__idle.is_set()
        
    def abort(self, wait=True):
        """Abort the currently ongoing task, if any."""
        self._abort = True
        if wait:
            while True:
                self._rob_fac.wait_for_control()
                if (np.linalg.norm(self._rob_fac.cmd_joint_increment) 
                    / self._rob_fac.control_period) < 1.e-3:
                    break
        
    def is_running(self):
        """Query if the controller is running, i.e. if it has a task.
        """
        return self.__running.is_set()
    
    def is_idle(self):
        """Query if the controller is idle, i.e. will accept new
        tasks.
        """
        return not self.__running.is_set()

    def _ramp_step(self):
        """Perform one interpolation step."""
        with self._llc_lock:
            ctf = self._frame_comp(self._rob_fac.cmd_joint_pos)
            invjac = self._frame_comp.inverse_jacobian(
                rcond=self._singular_cutoff)
            llc_time = self._llc_notify_time
        period_est = self._rob_fac.control_period
        path_pos = self._metric_(self._trf0, ctf)
        if self._abort and not self._aborting:
            self._aborting = True
            self._brake_pos = min(path_pos, self._brake_pos)
            self._stop_pos = min(self._path_length, self._acc_length + path_pos)
        ## Determine if we are ramping up, going flat, or ramping down.
        if (path_pos <= self._acc_length 
               and path_pos < self._brake_pos):
            ## Ramping up
            v = min(self._speed, math.sqrt(2 * path_pos * self._ramp_accel))
            if v == 0.0:
                ## Make sure we actually get an initial speed
                v = self._ramp_accel * period_est
            new_path_pos = path_pos + period_est * v
            if self._log_level >= 5:
                self._log('Ramping UP at position %f and speed %f'%(path_pos, v), 5)
        if path_pos >= self._brake_pos:
            ## Ramping down
            vsqr = 2 * (self._stop_pos - path_pos) * self._ramp_accel
            if vsqr <= 0:
                v = 0.0
                ## We're done (should be!)
                self._log('Task reached', 3)
                new_path_pos = path_pos
            else:
                v = math.sqrt(vsqr)
                new_path_pos = path_pos + period_est * v
            if self._log_level >= 5:
                self._log('Ramping DOWN at position %f and speed %f' %
                      (path_pos, v), 5)
        if path_pos < self._brake_pos and path_pos > self._acc_length:
            ## Going on target velocity
            new_path_pos = path_pos + period_est * self._speed
            if self._log_level >= 5:
                self._log('%.4f : Constant speed at position %f and speed %f' %
                      (llc_time, path_pos, self._speed), 5)
        ## Compute joint step to new path position
        if path_pos >= self._stop_pos + self._stopping_accuracy:
            if self._log_level >= 2:
                self._log('!! WARNING !! : Got path position exceeding ' +
                          'path length by more than the stopping accuracy!'
                          , 2)
        if new_path_pos < path_pos:
            if self._log_level >= 2:
                self._log('!! WARNING !! : Got new path position ' +
                          'preceeding the current one!', 2)
        ## Get the new task space target by the interpolator
        ttf = self._tli(min(1.0, new_path_pos / self._path_length))
        dpose = (ctf.inverse * ttf).pose_vector
        dtrf = ctf.orient * dpose
        #dtrf = framecomputer.trfdisp(ctf, ttf)
        dq = np.dot(invjac, dtrf)
        # # // Scale the joint step linearly, so that it does not give a
        # # // longer than required task step
        # otf = self._frame_comp(dq + self._rob_fac.cmd_joint_pos)
        # dStep = self._metric_(ctf, otf)
        # dTarget = self._metric_(ctf, ttf)
        # if dTarget < dStep:
        #     dq *= dTarget/dStep
        # // Execute the joint step
        self._rob_fac.cmd_joint_increment = dq
        # // Compute the obtainable tool flange for this step and
        # // abort, if the tolerance is violated
        if self._trajectory_tolerance > 0.0:
            otf = self._frame_comp(dq + self._rob_fac.cmd_joint_pos)
            if otf.pos.dist(ttf.pos) > self._trajectory_tolerance:
                self._log('!! Error !! : Trajectory position tolerance exceeded. ' +
                          'Aborting!', 0)
        # // Check if the joint step was scaled
        # dqs = self._rob_fac.cmd_joint_increment
        # if np.sum((dq - dqs) ** 2) > 0.0001:
        #     #ctfnew = self._frame_comp(dqs + self._llcQSer)
        #     ctfnew = self._frame_comp(dqs + self._rob_fac.cmd_joint_pos)
        #     new_path_pos = self._trf0._v.dist(ctfnew._v)
        #     if self._log_level >= 3:
        #         self._log('Joint increment retarded due to speed limit', 3)
        ## If reached, go idle
        if path_pos >= self._stop_pos - self._stopping_accuracy:
            self._set_idle()
            self._t_reached = True
        ## Test if the computation went past the deadline
        if self._llc_event.is_set():
            if self._log_level >= 2:
                self._log('!!! LLC event happened before this cycle. !!!', 2)
            if self._drop_frames:
                if self._log_level >= 2:
                    self._log('!!! Dropping control frame. !!!', 2)
                self._llc_event.clear()
        
    def _pos_metric(self, t0, t1):
        return  t0._v.dist(t1._v)

    def _ang_metric(self, t0, t1): 
        return t0._o.ang_dist(t1._o)

    def set_target_pose(self, 
                target_pose, linear_speed=None, angular_speed=None,
                ramp_accel=None, do_scale=True, express_frame='Base'):
        """Perform a linear motion from current tool pose to the given
        'target_pose', limited by 'linear_speed' [m/s] or
        'angular_speed' [rad/s] and a 'ramp_accel' [m/s^2] or
        [rad/s^2] acceleration for ramping the speed up and down. The
        'express_frame' determines in what reference the 'target_pose'
        is given; either 'Tool' or 'Base'. Here 'Tool' means the
        instantaneous tool reference frame at the start of the motion;
        i.e. not the moving tool frame during the motion. Caution: Be
        very careful and aware with the 'express_frame' parameter, and
        be certain that it corresponds to the 'target_pose'!
        """
        ## Setup, check, and start task
        with self._task_lock:
            if self.is_running():
                # Reject setting new target while running.
                task_accepted = False
            else:
                self._log('Accepting task at time %d' % time.time(), 4)
                self._do_scale = do_scale
                self._t_reached = False
                self._abort = False
                self._aborting = False
                self._linear_speed = (linear_speed if not linear_speed is None 
                                      else self._dflt_lin_spd)
                self._angular_speed = (angular_speed if not angular_speed is None 
                                      else self._dflt_ang_spd)
                self._ramp_accel = (ramp_accel if not ramp_accel is None 
                                      else self._dflt_ramp_acc)
                self._trf0 = self._frame_comp(self._rob_fac.cmd_joint_pos)
                if express_frame == 'Tool':
                    ## Get base representation of the tool target.
                    target_pose = self._trf0 * target_pose
                self._trf1 = m3d.Transform(target_pose)
                self._log('Current pose : %s' % str(self._trf0), 4)
                self._log('Target pose : %s' % str(self._trf1), 4)
                ang_dist = self._trf0.orient.ang_dist(self._trf1.orient)
                lin_dist = self._trf0.pos.dist(self._trf1.pos)
                # Determine if motion is angular or linear dominated
                # by time required for motion
                if (0.5 * self._arm_scale * ang_dist / self._angular_speed 
                    > lin_dist/self._linear_speed):
                    self._metric_ = self._ang_metric
                    self._speed = self._angular_speed
                    self._log('Angular bounded motion.', 4)
                else:
                    self._metric_ = self._pos_metric
                    self._speed = self._linear_speed
                    self._log('Linearly bounded motion.', 4)
                self._path_length = self._metric_(self._trf0, self._trf1)
                self._stop_pos = self._path_length
                if self._path_length < self._stopping_accuracy:
                    ## Do not accept a task with end point within the
                    ## stopping accuracy
                    task_accepted = False
                else:
                    self._tli = m3d.interpolation.SE3Interpolation(
                        self._trf0, self._trf1)
                    self._acc_length = 0.5 * self._speed ** 2 / self._ramp_accel
                    self._brake_pos = (self._path_length - self._acc_length 
                                     - self._speed * self._rob_fac.control_period)
                    self._log('Starting : BP=%.3f PL=%.3f AL=%.3f' %
                              (self._brake_pos, self._path_length,
                               self._acc_length), 3)
                    if 2*self._acc_length > self._path_length:
                        self._brake_pos = 0.5*self._path_length
                    self._tstart = time.time()
                    self._set_running()
                    task_accepted = True
        return task_accepted

    def llc_notify(self, event_time):
        """Custom handler for LLC notification overriding the one in
        the Controller base class. Here garbage collection is disabled
        and must be enabled again by the response handler.
        """
        gc.disable()
        with self._llc_lock:
            self._llc_notify_time = event_time
            self._llc_event.set()

    def run(self):
        if self._start_running:
            self.resume()
            self._log('Started running', 4)
        else:
            self._log('Started suspended', 4)
        self._stop_flag = False
        self._set_idle()
        while not self._stop_flag:
            self._llc_event.wait()
            if self._stop_flag:
                break
            self._llc_event.clear()
            t_start=time.time()
            if self.is_running():
                self._ramp_step()
                if self._sample_times > 0:
                    t_end = time.time()
                    self._comp_times = np.roll(self._comp_times,1)
                    self._comp_times[0] = t_end - t_start                    
            else:
                self._rob_fac.cmd_joint_increment = self._dq_zero
            gc.enable()
        # // Guard
        gc.enable()
        self._exit_event.set()
        self.suspend()

    def stop(self):
        self._stop_flag = True
        # Make sure to wake up
        self._llc_event.set()
        # Wait for control completion
        self._exit_event.wait()
