# coding=utf-8
"""
Module with a base class interface for a control facility. A control
facility is an object used by a legacy facade to send and track
control commands to a robot system.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import traceback
import threading

import numpy as np

class ControlFacility(object):
    """ Abstract base class for a legacy control facility. A
    specialized concrete implementation must provide data members
    '_q_tracked', '_q_increment', and '_v_tracked', in addition to the
    given methods."""

    def __init__(self, **kwargs):
        self._initialized = threading.Event()
        self._log_level = kwargs.get('log_level', 2)
        self._rob_fac = kwargs.get('robot_facade')
        self._rob_def = self._rob_fac.robot_definition
        # # Declaration of tracked state variables
        self._v_tracked = None
        self._q_tracked = None
        self._q_increment = None
        
    def _log(self, msg, level=2):
        if level <= self._log_level:
            print(str(level) + ' : ' + self.__class__.__name__ 
                  + '::' + traceback.extract_stack()[-2][2]
                  + ' : ' + msg)
    
    def _update_tracking(self, q_ser_new):
        self._q_increment = q_ser_new - self._q_tracked
        self._v_tracked = self._q_increment / self._rob_fac.control_period
        self._q_tracked = q_ser_new.copy()

    def _set_serial_config(self, q_ser, do_scale=True):
        raise NotImplementedError(traceback.extract_stack(limit=1)[-1][2] +'"'
                                  +' not implemented in abstract base class "ControlFacility"')

    def _set_serial_increment(self, dq_ser, do_scale=True):
        raise NotImplementedError(traceback.extract_stack(limit=1)[-1][2] +'"'
                                  +' not implemented in abstract base class "ControlFacility"')

    def wait_for_initialized(self):
        self._initialized.wait()

    def initialize(self):
        self._log('Waiting for robot_facade to initialize connection.', 4)
        self._rob_fac.wait_for_connection_initialized()
        self._log('Initializing tracked state variables.', 4)
        self._q_tracked = self._rob_fac.act_joint_pos
        self._q_increment = np.zeros(len(self._q_tracked))
        self._v_tracked = np.zeros(len(self._q_tracked))
        self._initialized.set()
        self._log('Initialized.', 3)
        self._log('v_tracked = %s' % str(self._v_tracked), 5)
        self._log('q_tracked = %s' % str(self._q_tracked), 5)

