# coding=utf-8

"""
Module for base class of all motion controllers. 
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import traceback
import string
import threading

import numpy as np

import pymoco.control
import pymoco.input


class Controller(object):
    """A controller base class providing some simple common mechanism
    to receive LLC data from the LLC publisher and send joint
    updates to the LLC."""

    class Error(Exception):
        """Error class for thowing exceptions."""
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        """Use the given llc publisher for reading input and open a
        socket to write to the LLC on the given 'llchost' address and
        'llcInPort'. Broadcast if 'broadCast' is true.
        """
        self._start_running = kwargs.get('start_running', True)
        self._rob_fac = kwargs.get('robot_facade')
        self._rob_def = self._rob_fac.robot_definition
        self._frame_comp = self._rob_fac.frame_computer
        self._log_level = kwargs.get('log_level', 2)
        self._llc_event = threading.Event()
        self._llc_lock = threading.RLock()
        self._llc_notify_time = -1.0
        self._exit_event = threading.Event()
        self._dq_zero = np.zeros(self._rob_def.dof, dtype=float)

    def resume(self):
        self._rob_fac.subscribe(self.llc_notify)
        self._log('Resumed',4)

    def suspend(self):
        self._rob_fac.unsubscribe(self.llc_notify)

    def llc_notify(self, event_time):
        """Standard handler for LLC notification. Status data from the
        LLC are stored in member variable '_llc_notify_data' and the
        '_llc_event' is set.
        """
        with self._llc_lock:
            self._llc_notify_time = event_time
            self._llc_event.set()

    def _log(self, msg, log_level=2):
        if log_level <= self._log_level:
            print(str(log_level) + ' : ' + self.__class__.__name__ \
                  + '::' + traceback.extract_stack()[-2][2] + ' : ' + msg)
