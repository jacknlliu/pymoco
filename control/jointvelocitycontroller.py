# coding=utf-8

"""
Module for joint velocity control.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import time

import numpy as np

from .controller import Controller 
from . import inf_norm

class JointVelocityController(Controller, threading.Thread):
    """Simple class to perform linear interpolation and take target
    positions in tool flange coordinates.
    """

    class Error(Exception):
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        threading.Thread.setDaemon(self, 1)
        Controller.__init__(self, **kwargs)
        self._timeout = kwargs.get('timeout', 0.1)
        self._last_command_time = time.time() - self._timeout
        self._timeout_flag = threading.Event()
        self._timeout_flag.set()
        self._stop_flag = False
        ## Assume that the robot is at rest initially
        self._vq_cmd = np.zeros(self._rob_def.dof, dtype=float)

    def get_timeout(self):
        return self._timeout
    def set_timeout(self, timeout):
        self._timeout = timeout
    timeout = property(get_timeout, set_timeout)
        
    def get_velocity(self):
        """Return the currently commanded joint velocity."""
        return self._vq_cmd
    def set_velocity(self, vq_cmd):
        """ Set the commanded joint velocity."""
        if not type(vq_cmd) == np.ndarray:
            vq_cmd =np.array (vq_cmd)
        with self._llc_lock:
            self._vq_cmd = vq_cmd
        self._last_command_time = time.time()
        self._timeout_flag.clear()
    velocity = property(get_velocity, set_velocity)
    cmd_velocity = velocity


    def wait_for_idle(self, timeout=None):
        """Block until the timeout flag is set, signalling that the
        controller is then idle.
        """
        if timeout is None:
            self._timeout_flag.wait()
        else:
            self._timeout_flag.wait(timeout)
        return self._timeout_flag.is_set()

    def abort(self, wait=True):
        """Abort the twist task currently running. This is achieved by
        manipulating last command time, so that a timeout is detected.
        """
        # Make the target 0
        self.velocity = (0,0,0,0,0,0)
        # Trigger timeout
        self._last_command_time = time.time() - self._timeout - 1
        if wait:
            while True:
                self._rob_fac.wait_for_control()
                if (np.linalg.norm(self._rob_fac.cmd_joint_increment) 
                    / self._rob_fac.control_period) < 1.e-3:
                    break

    def get_is_idle(self):
        """Query the idle-state of the controller. The controller is
        idle when the last control command hast timed out.
        """
        return self._timeout_flag.is_set()
    is_idle = property(get_is_idle)


    def run(self):
        if self._start_running:
            self.resume()
            self._log('Started running', 4)
        else:
            self._log('Started suspended', 4)
        self._stop_flag = False
        while not self._stop_flag:
            self._llc_event.wait()
            if self._llc_event.is_set() and not self._stop_flag:
                self._llc_event.clear()
                if (time.time() - self._last_command_time < self._timeout
                    and not self._timeout_flag.is_set()):
                    ## Compose new target
                    if self. _vq_cmd is not None:
                        with self._llc_lock:
                            dq = self._vq_cmd * self._rob_fac.control_period
                        self._rob_fac.cmd_joint_increment = dq
                        dq_actual = self._rob_fac.cmd_joint_increment
                        if inf_norm(dq_actual - dq) > 0.001:
                            self._log('Warning: Desired increment could not ' +
                                      'be set. Deviation: {}'
                                      .format(str(dq_actual - dq)),1)
                else:
                    if self._log_level >= 4: 
                        self._log('Watchdog timeout at {:#.2f}'
                                  .format(time.time()), 4)
                    self._timeout_flag.set()
                    # Beware: No braking, simply stop
                    self._rob_fac.cmd_joint_increment = self._dq_zero
            else:
                self._rob_fac.cmd_joint_increment = self._dq_zero
        self._exit_event.set()
        self.suspend()

    def stop(self):
        """ Stop thread activity."""
        self._stop_flag = True
        # Suppress any further motion
        self._timeout_flag.set()
        # Make sure to wake up
        self._llc_event.set()
        # Wait for controller to complete
        self._exit_event.wait()
