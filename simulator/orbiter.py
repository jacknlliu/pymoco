# coding=utf-8
"""
Module for getting mouse hit, and showing mouse in BGE.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import site

import math3d as m3d
import numpy as np

from  GameLogic import getCurrentController,getCurrentScene
import GameLogic
from time import time
import Rasterizer
Rasterizer.showMouse(1)


#from robot_emu import obj_name

class MouseBallMover:
    def __init__(self):
        scene = GameLogic.getCurrentScene()
        gos = scene.objects
        #self._cGO = gos['OBMouseControl']
        self._ballGO = gos['ball']
        self._cGO = self._ballGO
        self._mouseLeft = self._cGO.sensors['mclkleft']
        self._mouseRight = self._cGO.sensors['mclkright']
        self._mouseMiddle = self._cGO.sensors['mclkmiddle']
        self._ctlKey = self._cGO.sensors['ctrlkey']
        self._shiftKey = self._cGO.sensors['shiftkey']
        self._eKey = self._cGO.sensors['ekey']
        self._qKey = self._cGO.sensors['qkey']
        self._mouseOver = self._cGO.sensors['mover']
        self._mouseMove = self._cGO.sensors['mmove']
        self._mouseWheelUp = self._cGO.sensors['wheelup']
        self._mouseWheelDown = self._cGO.sensors['wheeldown']
        self._ballAct = self._ballGO.actuators['rotator']
        ## Position and orient ball and parent to camera
        self._camGO = scene.active_camera
        camorient = self._camGO.worldOrientation
        self._ballGO.worldOrientation = camorient
        camorient = m3d.Orientation(np.array(self._camGO.worldOrientation))
        campos = m3d.Vector(np.array(self._camGO.worldPosition))
        self._ballGO.worldPosition = (campos - 2 * camorient.vec_z).array
        self._camGO.setParent(self._ballGO)

    def manipBall(self):
        if self._mouseMiddle.positive or self._mouseRight.positive:
            if self._mouseMiddle.triggered or self._mouseRight.triggered:
                self._mouseMove.frequency = 0
                self._pos = self._mouseMove.position
            else:
                pos = self._mouseMove.position
                dx = pos[0] - self._pos[0]
                dy = pos[1] - self._pos[1]
                if self._mouseMiddle.positive:
                    zworld = m3d.Orientation(
                        np.array(self._ballGO.worldOrientation)).inverse.vec_z
                    self._ballAct.dRot = (m3d.Vector(-0.005*dy,0,0) 
                                          - 0.005*dx*zworld)._data
                    GameLogic.getCurrentController().activate(self._ballAct)
                elif self._mouseRight.positive:
                    ballPos = m3d.Vector(np.array(self._ballGO.worldPosition))
                    ballOrientation = m3d.Orientation(
                        np.array(self._ballGO.worldOrientation))
                    camDist = ballPos.dist(m3d.Vector(
                            np.array(self._camGO.worldPosition)))
                    scale = 0.001 * camDist
                    ballPos -= dx * scale * ballOrientation.vec_x
                    if self._shiftKey.positive:
                        ballPos += dy * scale * m3d.Vector.e2
                    else: 
                    #elif self._ctlKey.positive:
                        ballPos += dy * scale * ballOrientation.vec_y
                    self._ballGO.worldPosition = ballPos.array
                self._pos = pos
        else:
            self._mouseMove.frequency = 20
            self._ballAct.dRot = [0, 0, 0]
                
    def putBall(self):
        if self._mouseLeft.positive:
            pos = [0,0,0]
            if self._mouseLeft.triggered:
                pos = self._mouseOver.hitPosition
            if pos != [0,0,0]:
                self._ballGO.worldPosition = pos
            if self._ctlKey.positive:
                self._ballGO.setParent(self._mouseOver.hitObject)
            else:
                self._ballGO.removeParent()

    def camInOut(self):
        if self._mouseWheelUp.triggered or self._qKey.triggered:
            scale = 1.0
        elif self._mouseWheelDown.triggered or self._eKey.triggered:
            scale = -1.0
        camPos = m3d.Vector(np.array(self._camGO.worldPosition))
        dvec = m3d.Vector(np.array(self._ballGO.worldPosition)) - camPos
        camPos += scale * dvec / 10.0
        self._camGO.worldPosition = camPos.array

def init():
    global mouse, put_ball, manip_ball, cam_in_out
    mouse = MouseBallMover()
    put_ball = mouse.putBall
    manip_ball = mouse.manipBall
    cam_in_out = mouse.camInOut
