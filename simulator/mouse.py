# coding=utf-8
"""
Module for mouse view control in BGE.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys

import GameLogic
import Rasterizer
Rasterizer.showMouse(1)

from .robot_emu import obj_name

def _getPosition25(go):
    return go.worldPosition
def _getOrientation25(go):
    return go.worldOrientation
def _getPosition24(go):
    return mathutils.Vector(go.worldPosition)
def _getOrientation24(go):
    return mathutils.Matrix(*go.worldOrientation)


if sys.version_info[0]==2:
    global getOrientation, getPosition
    import Mathutils as mathutils
    getPosition = _getPosition24
    getOrientation = _getOrientation24
elif sys.version_info[0]==3:
    global getOrientation, getPosition
    import mathutils
    getPosition = _getPosition25
    getOrientation = _getOrientation25
    
class MouseBallMover:
    def __init__(self):
        gos = GameLogic.getCurrentScene().objects
        self._cGO = gos[obj_name('MouseControl')]
        self._camGO = gos[obj_name('SceneCamera')]
        self._ballGO = gos[obj_name('ball')]
        self._mouseLeft = self._cGO.sensors['mclkleft']
        self._mouseRight = self._cGO.sensors['mclkright']
        self._mouseMiddle = self._cGO.sensors['mclkmiddle']
        self._ctlKey = self._cGO.sensors['ctrlkey']
        self._shiftKey = self._cGO.sensors['shiftkey']
        self._eKey = self._cGO.sensors['ekey']
        self._qKey = self._cGO.sensors['qkey']
        self._mouseOver = self._cGO.sensors['mover']
        self._mouseMove = self._cGO.sensors['mmove']
        self._mouseWheelUp = self._cGO.sensors['wheelup']
        self._mouseWheelDown = self._cGO.sensors['wheeldown']
        self._ballAct = self._ballGO.actuators['rotator']
        #print(self._ballGO.controllers)
        #self._ballCtl = self._ballGO.controllers['rotatectl#CONTR#13']
        
    def manipBall(self):
        if self._mouseMiddle.positive or self._mouseRight.positive:
            if self._mouseMiddle.triggered or self._mouseRight.triggered:
                self._mouseMove.frequency = 0
                self._pos = self._mouseMove.position
            else:
                pos = self._mouseMove.position
                dx = pos[0] - self._pos[0]
                dy = pos[1] - self._pos[1]
                if self._mouseMiddle.positive:
                    zworld = getOrientation(self._ballGO)[2]
                    rotZ = -0.005*dx*zworld
                    rotX = mathutils.Vector((0.005*dy, 0.0, 0.0))
                    self._ballAct.dRot = list(rotX + rotZ)
                    GameLogic.getCurrentController().activate(self._ballAct)
                elif self._mouseRight.positive:
                    ballPos = getPosition(self._ballGO)
                    camPos = getPosition(self._camGO)
                    ballOrientation = getOrientation(self._ballGO)
                    ballOrientation.invert()
                    camDist = (ballPos-camPos).length
                    scale = 0.002 * camDist
                    ballPos += dx * scale * ballOrientation[0]
                    if self._shiftKey.positive:
                        ballPos += dy * scale * mathutils.Vector((0.0, 0.0, 1.0))
                    else:
                        ballPos += dy * scale * ballOrientation[2]
                    self._ballGO.worldPosition = list(ballPos)
                self._pos = pos
        else:
            self._mouseMove.frequency = 20
            self._ballAct.dRot = [0.0, 0.0, 0.0]
                
    def putBall(self):
        if self._mouseLeft.positive:
            pos = [0,0,0]
            if self._mouseLeft.triggered:
                pos = self._mouseOver.hitPosition
            if pos != [0,0,0]:
                self._ballGO.worldPosition = pos
            if self._ctlKey.positive:
                self._ballGO.setParent(self._mouseOver.hitObject)
            else:
                self._ballGO.removeParent()

    def camInOut(self):
        if self._mouseWheelUp.triggered or self._qKey.triggered:
            scale = 1.0
        elif self._mouseWheelDown.triggered or self._eKey.triggered:
            scale = -1.0
        camPos = getPosition(self._camGO)
        dvec = getPosition(self._ballGO) - camPos
        camPos += scale * dvec / 10.0
        self._camGO.worldPosition = list(camPos)



def init():
    global mouse, camInOut, putBall, manipBall
    mouse = MouseBallMover()
    camInOut = mouse.camInOut
    putBall = mouse.putBall
    manipBall = mouse.manipBall
