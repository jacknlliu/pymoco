# coding=utf-8
"""
Module for setting up emulation of the Universal Robots UR-6-85-5-A
robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012-2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import types
import struct
import socket
import time
import traceback

import numpy as np
import math3d as m3d

import GameLogic
import pymoco.control
import pymoco.input
import pymoco.kinematics
import pymoco.robot

from pymoco.simulator.robot_emu import find_link_chain, obj_name


class URRouterUDPEmu(threading.Thread):

    # // Structs for transmission of joint position data. The out struct  
    p_struct_out = struct.Struct('<BL6d6d')
    p_struct_in = struct.Struct('<BL6d')
  
    def __init__(self, rob_base_go, rob_def, bind_host='127.0.0.1',
                 gw_in_port=pymoco.control.llc_in_port,
                 gw_out_port=pymoco.input.llc_out_port,
                 log_level=2):
        self._stop_flag = False
        # # Socket port for receiving commanded joint positions
        self._gw_in_port = gw_in_port
        # # Socket port for emitting actual joint positions
        self._gw_out_port = gw_out_port
        self._log_level=log_level
        self._bind_host = bind_host
        # // The game object for the robot base 
        self._base_go = rob_base_go
        self._rob_def = rob_def
        threading.Thread.__init__(self)
        self._data_lock = threading.Lock()
        self._cycle_number = 0
        self._q = self._rob_def.q_home.copy()
        self._last_q = self._q.copy()
        # // Base transform and frame computer
        self._base_xform=m3d.Transform(
          m3d.Orientation(np.array(self._base_go.worldOrientation)),
          m3d.Vector(np.array(self._base_go.worldPosition)))
        self._log('Found robot base at position %s' %
              str(self._base_xform._v))
        self._fc = pymoco.kinematics.FrameComputer(
            q=np.zeros(6), base_xform=self._base_xform,
            rob_def=self._rob_def, cache_in_frames=True)
        self._log('Kinematics report tool position at %s' %
              str(self._fc()._v))
        self._links = find_link_chain(self._base_go)
        self._log('Robot links: %s' % str(self._links))
        self._set_q(self._q)
        self._gw_in_socket=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._gw_in_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._gw_in_socket.bind((self._bind_host, self._gw_in_port))
        self._gw_in_socket.settimeout(0.005)
        self._gw_out_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        if self._bind_host in ['localhost', '127.0.0.1']:
            self._gw_out_addr = ('127.0.0.1', self._gw_out_port)
        else:
            self._gw_out_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            self._gw_out_addr = ('255.255.255.255', self._gw_out_port)
        self._gw_out_socket.connect(self._gw_out_addr)
        self._log('Syncing to address ' + str(self._gw_out_addr))
        self._t_cur = time.time()
        self._t_last_log = self._t_cur
        self._period_est = 0.01
        self._t_old = self._t_cur - self._period_est
        
    def _log(self, msg, log_level=2):
        if log_level <= self._log_level:
            print(str(log_level) + ' : ' + self.__class__.__name__
                  + ' (<%d,>%d)::' % (self._gw_in_port,self._gw_out_port)
                  + traceback.extract_stack(limit=2)[-2][2] + ' : ' + msg)

    def _set_q(self, q):
        self._fc.joint_angles_vec = q
        frames = self._fc.get_in_frame(array=True)
        for l, f in zip(self._links, frames):
            l.worldOrientation = f[:3,:3].tolist()
            l.worldPosition = f[:3,3].tolist()

    def emit_state(self):
        if self._stop_flag:
            return
        # // Pack the stom package
        with self._data_lock:
            q = self._q.copy()
        v = (self._q-self._last_q)/self._period_est
        # // Update cycle number
        self._cycle_number += 1
        # // Pack and send
        self._t_cur = time.time()
        try:
            self._gw_out_socket.send(self.p_struct_out.pack(*([0x00, self._cycle_number] + q.tolist() + v.tolist())))
        except socket.error:
            if self._t_cur - int(self._t_cur) < self._period_est:
                self._log('Could not send (emit) on out-socket! Nobody listening?', 3)
        self._period_est = 0.5 * self._period_est + 0.5 * (self._t_cur - self._t_old)
        if self._t_cur - self._t_last_log > 10:
            self._log('Frame-rate est. : %.2f' % (1.0 / self._period_est), 4)
            self._t_last_log = time.time()
        self._t_old = self._t_cur
        time.sleep(0.001)

    def _test_recv_pos(self):
        try:
            data = self._gw_in_socket.recv(1024)
        except socket.error:
            pass
        else:
            if len(data) == self.p_struct_in.size:
                data_vec = self.p_struct_in.unpack(data)
                cycle_number_ack = data_vec[1]
                if cycle_number_ack != self._cycle_number:
                    self._log('Warning: Cycle number mis-match: %d != %d'
                              % (cycle_number_ack, self._cycle_number), 1)
                q = np.array(data_vec[2:])
                self._set_q(q)
                with self._data_lock:
                    self._last_q = self._q
                    self._q = q
                    if self._log_level >= 5: self._log('Received position:'+str(self._q), 5)
            else:
                self._log('Warning: Wrong packet size received. Got %d, expected %d'
                          % (len(data), self.p_struct_in.size),1)
                
    def run(self):
        self._initializing = True
        self._stop_flag = False
        emits = 0
        while self._initializing and emits < 10 and not self._stop_flag:
            self.emit_state()
            time.sleep(0.1)
            emits += 1
        self._initializing = False
        while not self._stop_flag:
            self._test_recv_pos()
        self._gw_out_socket.close()
        self._gw_in_socket.close()
        self._log('Stopped.',1)

    def stop(self):
        self._stop_flag = True

LOG_LEVEL = 2
__ur_emu__ = None
time_update = None

def init():
    global __ur_emu__, time_update
    basename = obj_name('UR6855A.BasePlate')
    scene = GameLogic.getCurrentScene()
    tlFileName = None
    ## Find base and set up emulator.
    base_go = None
    for o in scene.objects:
        if o.name.find(basename)>=0:
            base_go = o
            print('Found base object: "%s"'%base_go.name)
            break
    if not base_go is None:
        __ur_emu__ = URRouterUDPEmu(
            base_go,
            pymoco.robot.create_robot('UR5_ML_CR', cylinder_rolls=[0.0,0.0]),
            log_level=LOG_LEVEL)
        time_update = __ur_emu__.emit_state
        __ur_emu__.start()
    else:
        print('Error: No base object found, emulator not started!')

_shutdown_event = threading.Event()

def shutdown():
    if not _shutdown_event.isSet():
        _shutdown_event.set()
        print('Shutting down ur emu.')
        __ur_emu__.stop()
        co = GameLogic.getCurrentController()
        actuator = co.actuators["exit"]
        co.activate(actuator)

