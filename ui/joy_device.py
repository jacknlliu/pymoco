# coding=utf-8
"""
Module for joystick input.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import struct
import threading

import numpy as np

JS_EVENT_BUTTON = 0x01  # button pressed/released
JS_EVENT_AXIS = 0x02 # joystick moved 
JS_EVENT_INIT = 0x80 # initial state of device

eventfmt = struct.Struct('IhBB')
eventsz = eventfmt.size


class JoyDevice(threading.Thread):
    def __init__(self, axes_flips=-1, scale_axes = True, verbose=False, 
                 js_dev_name=None):
        # /dev/input/by-id/usb-Sony_PLAYSTATION_R_3_Controller-joystick'):
        if os.path.exists(js_dev_name):
            self._js_dev_name = js_dev_name
        else:
            if js_dev_name != '':
                print('Given joystick ("%s") does not exist' % js_dev_name)
            jdevs = [fn for fn in os.listdir('/dev/input/') if fn[:2] == 'js']
            if len(jdevs) > 0:
                self._js_dev_name = os.path.join('/dev/input/', 'js0')
            else:
                raise Exception('No joystics found!')
        if os.path.exists(js_dev_name):
            self._js_dev_name = js_dev_name
        else:
                print('Given joystick ("%s") does not exist' % js_dev_name)
        print('Using joystick: %s' % self._js_dev_name)
        threading.Thread.__init__(self)
        self.daemon = True
        self._verbose = verbose
        self._axes_max = float(np.iinfo(np.short).max)
        self.__stop = False
        self._event_condition = threading.Condition()
        self._axis_event_condition = threading.Condition()
        self._button_event_condition = threading.Condition()
        self._initialize()
        if type(scale_axes) != np.ndarray:
            scale_axes = np.array([scale_axes]*self._n_axes)
        self._scale_axes = scale_axes
        if type(axes_flips) != np.ndarray:
            axes_flips = np.array([axes_flips]*self._n_axes)
        self._axes_flips = axes_flips

    def get_axes(self):
        return self._axes.copy()
    axes = property(get_axes)

    def get_axis_event(self):
        return self._a_ev
    axis_event= property(get_axis_event)

    def get_buttons(self):
        return self._buttons.copy()
    buttons = property(get_buttons)

    def get_button_event(self):
        return self._b_ev
    button_event = property(get_button_event)


    def wait_for_button_event(self):
        with self._button_event_condition:
            self._button_event_condition.wait()
            
    def wait_for_axis_event(self):
        with self._axis_event_condition:
            self._axis_event_condition.wait()
            
    def wait_for_event(self, timeout=None):
        with self._event_condition:
            if timeout is None or timeout < 0:
                return self._event_condition.wait()
            else:
                return self._event_condition.wait(timeout)
                
    def _initialize(self):
        print(self.__class__.__name__ 
              + ' Initializing. Press any button to finalize.')
        self._dev = open(self._js_dev_name,'rb')
        init_event = True
        init_axes = {}
        init_buttons = {}
        while init_event:
            ev = eventfmt.unpack(self._dev.read(eventsz))
            if not ev[2] & JS_EVENT_INIT:
                init_event = False
            else:
                if ev[2] & JS_EVENT_AXIS:
                    init_axes[ev[3]] = ev[1]
                elif ev[2] & JS_EVENT_BUTTON:
                    init_buttons[ev[3]] = ev[1]
        # if self._scale_axes:
        #     self._axes = np.zeros(len(init_axes), dtype=np.float)
        # else:
        #     self._axes = np.zeros(len(init_axes), dtype=np.int)
        self._axes = np.zeros(len(init_axes), dtype=np.float)
        self._buttons = np.zeros(len(init_buttons), dtype=np.bool)
        self._n_axes = len(self._axes)
        self._n_buttons = len(self._buttons)
        print(self._axes)
        print(self._buttons)
        #self._ev = ev

    def _print_state(self):
        print('% .1f '*self._n_axes % tuple(self._axes))
        print('\n')
        print('%1d '*self._n_buttons % tuple(self._buttons))
        print('\n')

    def run(self):
        self.__stop = False
        while not self.__stop:
            ev_time, ev_value, ev_type, ev_index = (
             eventfmt.unpack(self._dev.read(eventsz)))
            if ev_type == JS_EVENT_AXIS:
                ax = self._axes_flips[ev_index] * ev_value
                if self._scale_axes[ev_index]:
                    ax /= self._axes_max
                self._axes[ev_index] = ax
                self._a_ev = (ev_index, ax)
                with self._axis_event_condition:
                    self._axis_event_condition.notifyAll()
            if ev_type == JS_EVENT_BUTTON:
                self._buttons[ev_index] = ev_value
                self._b_ev = (ev_index, ev_value)
                with self._button_event_condition:
                    self._button_event_condition.notifyAll()
            with self._event_condition:
                self._event_condition.notifyAll()
            if self._verbose:
                self._print_state()
            
    def stop(self):
        self.__stop = True

class SixAxis(threading.Thread):
    def __init__(self, js_dev, flips,):
        self._jsd = js_dev
        

def _test():
    np.set_printoptions(linewidth=120)
    jdev = JoyDevice(verbose=True, js_dev_name='/dev/input/js0')
    jdev.start()
