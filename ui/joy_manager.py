# coding=utf-8
"""
Module for management class for joystick control of robots. 
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

from .joy_controller import JoyController
from .joy_device import JoyDevice

class JoyManager(object):
    def __init__(self, con_mans, js_dev_name=None, start_index=-1):
        self._jdev = JoyDevice(js_dev_name=js_dev_name)
        self._jdev.start()
        self._jc  = JoyController(jdev=self._jdev)
        self._con_mans = con_mans
        self._index = -1
        if start_index >= 0:
            self(start_index)
        
    def __call__(self, index=0):
        if index == self._index:
            return self._jc
        if not self._jc is None and self._jc.isAlive():
            print('Current Joy Controller must be stopped before switching!')
        else:
            self._index = index
            self._jc = JoyController(
                self._con_mans[self._index].tvc(), jdev=self._jdev)
            self._jc.start()

    def stop_joy(self):
        if self._index >= 0:
            print('Stopping Joy Controller. An event must be generated, '
                  + 'so press any button.')
            self._jc.stop()
            self._jc.join()
            self._index = -1


