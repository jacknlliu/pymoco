# coding=utf-8
"""
Module for control class for binding joystick control inputs to
velocity commands of an associated robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import struct
import threading
import time

import numpy as np

from .joy_device import JoyDevice

# Left stick axes
lh = 0
lv = 1
# Right stick axes
rh = 2
rv = 3
# Front left buttons
fll = 6
flu = 4
# Front right buttons
frl = 7
fru = 5

linear_scale = 0.075
rotation_scale = 0.3

class JoyController(threading.Thread):
    def __init__(self, tvc=None, jdev=None):
        threading.Thread.__init__(self)
        self.daemon = True
        self._control_lock = threading.Lock()
        if not tvc is None:
            self.resume(tvc)
            self._tvc.timeout = 10.0
        else:
            self._tvc = None
            self._suspended = True
        if jdev is None:
            self._jdev = JoyDevice()
            self._jdev.start()
        else:
            self._jdev = jdev
        self.__stop = False
        
    def run(self):
        while not self.__stop:
            if self._suspended:
                time.sleep(0.1)
                continue
            with self._control_lock:
                self._jdev.wait_for_event()
                axes = self._jdev.axes
                buttons = self._jdev.buttons
                if buttons[fll]:
                    twist = list(linear_scale*axes[[lh,lv,rv]])+[0,0,0]
                elif buttons[flu]:
                    twist = [0,0,0]+list(rotation_scale*axes[[lh,lv,rv]])
                else:
                    twist = np.zeros(6)
                # print(twist)
                expr_frm = 'Base'
                if buttons[fru]:
                    expr_frm = 'Tool'
                self._tvc.set_twist(twist, express_frame=expr_frm)
        if not self._suspended:
                self._tvc.set_twist([0,0,0,0,0,0])
                self._tvc.timeout = 0.1
    

    def suspend(self):
        self._suspended = True
        print('Suspending joystick control. '
              + 'Possibly press a button to wake up the joystick device.')
        with self._control_lock:
            self._tvc.set_twist([0,0,0,0,0,0])
            tvc = self._tvc
            self._tvc = None
            tvc.timeout = 0.1
            return tvc

    def resume(self, tvc):
        """Resume operation, using the given 'tvc'."""
        print('Resuming joystick control.')
        with self._control_lock:
            self._suspended = False
            tvc.timeout = 10.0
            self._tvc = tvc
        
    def get_jdev(self):
        return self._jdev
    jdev = property(get_jdev)

    def stop(self, join=False):
        self.__stop = True
        print('Joystick controller ready for stopping. Press a button to exit.')
        if join:
            self.join()
