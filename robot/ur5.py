# coding=utf-8
"""
Module with setup for the default model for the Universal Robots UR5.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

from .ur5_ml_cyl_rolls import UR5_ML_CylRolls
UR5 = UR5_ML_CylRolls
