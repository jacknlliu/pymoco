# coding=utf-8
"""
Initialization of the 'robot' package. Special factory methods for
getting instances of definitions for robot types.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012-2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

from .nachi_sh133 import NachiSH133
from .nachi_sc15f import NachiSC15F
from .ur5 import UR5
from .ur5_ml import UR5_ML
from .ur5_ml_cyl_rolls import UR5_ML_CylRolls
from .ur5_dh import UR5_DH

_robot_types = {'UR5':UR5, 'UR5_DH':UR5_DH, 'UR5_ML':UR5_ML, 
                'UR5_ML_CR': UR5_ML_CylRolls,
                'UR6855A':UR5, 'UR6855A_DH':UR5_DH, 'UR6855A_ML':UR5_ML,
                'NachiSC15F':NachiSC15F, 'NachiSH133':NachiSH133}

def has_robot_type(robot_type):
    return robot_type in _robot_types
def hasRobotType(robot_type):
    print('pymoco.robot : Deprecation warning : '
          + '"hasRobotType" is deprecated, use "has_robot_type".')
    return has_robot_type(robot_type)
    
def get_robot_types():
    return list(_robot_types)
def getRobotTypes():
    print('pymoco.robot : Deprecation warning : '
          + '"getRobotTypes" is deprecated, use "get_robot_types".')
    return get_robot_types()

def create_robot(robot_type, **kwargs):
    return _robot_types[robot_type](**kwargs)
def get_robot_type(robot_type):
    print('pymoco.robot : Deprecation warning : '
          + '"get_robot_type" is deprecated, use "create_robot".')
    return create_robot(robot_type)
def getRobotType(robot_type):
    print('pymoco.robot : Deprecation warning : '
          + '"getRobotType" is deprecated, use "create_robot".')
    return create_robot(robot_type)
